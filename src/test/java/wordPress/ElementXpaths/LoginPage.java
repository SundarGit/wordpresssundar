package wordPress.ElementXpaths;

/*
 * This class file consist of all HomePage elements xpath
 * Created on: 08-Aug-2021
 * for now only added the needed elements xpaths
 */

public class LoginPage {
	public String UserNameEmail = "//*[@id='usernameOrEmail']";
	public String Continue = "//button[text()='Continue']";
	public String Password = "//*[@id='password']";
	public String btnLogin = "//button[text()='Log In']";
	public String lnkLogin = "(//a[@title='Log In'])[1]";
}
