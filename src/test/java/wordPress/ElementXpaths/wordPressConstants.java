package wordPress.ElementXpaths;

public class wordPressConstants {

	public class LoginPageConstants {
		public static final String UserNameEmail = "//*[@id='usernameOrEmail']";
		public static final String Continue = "//button[text()='Continue']";
		public static final String Password = "//*[@id='password']";
		public static final String btnLogin = "//button[text()='Log In']";
		public static final String lnkLogin = "(//a[@title='Log In'])[1]";

	}

	public class LogoutPageConstants {

		public static final String btnLogout = "//button[text()='Log out']";
	}

	public class MyHomePageConstants {
		public static final String lblMyhome = "//*[contains(text(),'My Home')]";

	}

	public class MyProfilePageConstants {
		public static final String Myprofileicon = "//*[@alt='My Profile']";
		public static final String Myprofile = "//*[contains(text(),'My Profile')]";
		public static final String Firstname = "//*[@id='first_name']";
		public static final String Lastname = "//input[@id='last_name']";
		public static final String ProfileImage = "//*[@id='primary']/main/div[2]/div/div[1]";
		public static final String Publicdisplayname = "//input[@id='display_name']";
		public static final String ProfileImageTooltip = "//*[@id='primary']/main/div[2]/div/div[2]/button";
		public static final String Aboutme = "//textarea[@id='description']";
		public static final String HideToggle = "//*[@id='primary']/main/div[2]/form/fieldset[5]/div/div/span";
		public static final String Saveprofile = "//*[@id='primary']/main/div[2]/form/p/button";
		public static final String QuestionMark = "//*[@id='wpcom']/div/div[3]/button";
		public static final String Avatarname = "//*[@class='profile-gravatar__user-display-name']";
		public static final String Avatarid = "//*[@id='secondary']/ul/li/div[1]/div[2]";
		public static final String Logout = "//*[@id='secondary']/ul/li/div[2]/button";
		public static final String GravatarProfile = "//a[contains(text(),'Gravatar profile')]";
		public static final String GravatarCom = "//a[contains(text(),'Gravatar.com')]";
		public static final String tooltipGravatarlink = "(//a[contains(text(),'Gravatar')])[3]";
		public static final String profilePageLinks = "//a[contains(text(),'Gravatar')]";
		public static final String Profilelinksadd = "//*[@id='primary']/main/div[3]/div[2]/button/span";
		public static final String ProfilelinkAddsite = "//button[contains(text(),'Add WordPress Site')]";
		public static final String ProfilelinkAddurl = "//button[contains(text(),'Add URL')]";
		public static final String alrtNotice = "//*[@class='notice__text']";
		public static final String alrtNoticeClose = "//*[@class='notice__dismiss']";
		public static final String txtprofileaddurl = "//*[@name='value']";
		public static final String txtprofileaddurldesc = "//*[@name='title']";
		public static final String btnAddsite = "//button[text()='Add Site']";
		public static final String lblProfilelinktitleSite = "//span[contains(text(),'Site Title')]";
		public static final String lblProfilelinktitleURL = "//span[contains(text(),'Google')]";
		public static final String lnkProfilelinkURL = "//*[contains(text(),'Google')]";
		public static final String btnProfilelinkDelete = "(//*[@class='button profile-link__remove is-borderless'])[1]";
		public static final String chkSitebox = "//*[@name='site-196300880']";
		public static final String lnkSite = "//span[text()='Site Title']"; // based on added site
		public static final String lnkUrl = "//span[text()='Google']"; // based on added url
		public static final String btnCancelAddSite = "//button[contains(text(),'Cancel')]";

	}

}
