package wordPress.ElementXpaths;

public class MyProfilePage {
	public String Myprofileicon = "//*[@alt='My Profile']";
	public String Myprofile = "//*[contains(text(),'My Profile')]";
	public String Firstname = "//*[@id='first_name']";
	public String Lastname = "//input[@id='last_name']";
	public String ProfileImage = "//*[@id='primary']/main/div[2]/div/div[1]";
	public String Publicdisplayname = "//input[@id='display_name']";
	public String ProfileImageTooltip = "//*[@id='primary']/main/div[2]/div/div[2]/button";
	public String Aboutme = "//textarea[@id='description']";
	public String HideToggle = "//*[@id='primary']/main/div[2]/form/fieldset[5]/div/div/span";
	public String Saveprofile = "//*[@id='primary']/main/div[2]/form/p/button";
	public String QuestionMark = "//*[@id='wpcom']/div/div[3]/button";
	public String Avatarname = "//*[@class='profile-gravatar__user-display-name']";
	public String Avatarid = "//*[@id='secondary']/ul/li/div[1]/div[2]";
	public String Logout = "//*[@id='secondary']/ul/li/div[2]/button";
	public String GravatarProfile = "//a[contains(text(),'Gravatar profile')]";
	public String GravatarCom = "//a[contains(text(),'Gravatar.com')]";
	public String tooltipGravatarlink = "(//a[contains(text(),'Gravatar')])[3]";
	public String profilePageLinks = "//a[contains(text(),'Gravatar')]";
	public String Profilelinksadd = "//*[@id='primary']/main/div[3]/div[2]/button/span";
	public String ProfilelinkAddsite = "//button[contains(text(),'Add WordPress Site')]";
	public String ProfilelinkAddurl = "//button[contains(text(),'Add URL')]";
	public String alrtNotice = "//*[@class='notice__text']";
	public String alrtNoticeClose = "//*[@class='notice__dismiss']";
	public String txtprofileaddurl = "//*[@name='value']";
	public String txtprofileaddurldesc = "//*[@name='title']";
	public String btnAddsite = "//button[text()='Add Site']";
	public String lblProfilelinktitleSite = "//span[contains(text(),'Site Title')]";
	public String lblProfilelinktitleURL = "//span[contains(text(),'Google')]";
	public String lnkProfilelinkURL = "//*[contains(text(),'Google')]";
	public String btnProfilelinkDelete = "(//*[@class='button profile-link__remove is-borderless'])[1]";
	public String chkSitebox = "//*[@name='site-196300880']";
	public String btnLogout = "//button[text()='Log out']";
	public String lnkSite = "//span[text()='Site Title']"; // based on added site
	public String lnkUrl = "//span[text()='Google']"; // based on added url
	public String btnCancelAddSite = "//button[contains(text(),'Cancel')]";

}
