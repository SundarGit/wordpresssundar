package wordPress.Pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wordPress.ElementXpaths.wordPressConstants;
import wordPress.Utilities.BaseClass;
import wordPress.Utilities.ExtentReporting;

public class MyProfilePage extends BaseClass {

	HttpURLConnection huc = null;
	int respCode;
	LinkedHashMap<String, String> mapData;
	String randomAplbha = RandomStringUtils.randomAlphabetic(3);

	public void NavigatetoMyProfile() throws InterruptedException {
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Myprofileicon);
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Aboutme);
		ExtentReporting.logger.pass("Clicked on My profile icon");
		cmWAITforElement(wordPressConstants.MyProfilePageConstants.Myprofile);
		ExtentReporting.logger.pass("navigated to My Profile Section");
		String currentPage = driver.getTitle();
		ExtentReporting.logger.pass("The current Page title is : " + currentPage);
		assertTrue(currentPage.contains("Profile"));
	}

	public void VerifyProfilePageLinks() throws InterruptedException, IOException {
		cmSCROLLToElement(wordPressConstants.MyProfilePageConstants.Myprofile);		
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.ProfileImageTooltip);
		List<WebElement> links = driver
				.findElements(By.xpath(wordPressConstants.MyProfilePageConstants.profilePageLinks));
		Iterator<WebElement> it = links.iterator();
		while (it.hasNext()) {
			String url;
			url = it.next().getAttribute("href");
			System.out.println("checking the connection of url :" + url);
			huc = (HttpURLConnection) (new URL(url).openConnection());
			huc.setRequestMethod("HEAD");
			huc.connect();
			respCode = huc.getResponseCode();
			if (respCode >= 400) {
				System.out.println(url + " is not working");
				ExtentReporting.logger.pass(url + " is not working as we are getting response code as " + respCode);
			} else {
				System.out.println(url + " is working");
				ExtentReporting.logger.pass(url + " is working as we are getting response code as " + respCode);
			}
		}
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.ProfileImageTooltip);
	}

	public void EditProfileInfoUsingExcel() throws InterruptedException, IOException {
		String beforeChange;
		String afterChange;
		mapData = getExcelDataAsMap("EditProfile", "Sheet1");
		beforeChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Firstname, "value");
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.Firstname, mapData.get("FirstName")+randomAplbha);
		afterChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Firstname, "value");
		ExtentReporting.logger.pass("Value of FirstName is updated from :  \" "  + beforeChange + " \" --------> \" " + afterChange+" \" ");

		beforeChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Lastname, "value");
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.Lastname, mapData.get("LastName")+randomAplbha);
		afterChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Lastname, "value");
		ExtentReporting.logger.pass("Value of LastName is updated from :  \" "  + beforeChange + " \" --------> \" " + afterChange+" \" ");

		beforeChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Publicdisplayname, "value");
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.Publicdisplayname, mapData.get("PublicDisplayName")+randomAplbha);
		afterChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Publicdisplayname, "value");
		ExtentReporting.logger.pass("Value of PublicDisplayName is updated from :  \" "  + beforeChange + " \" --------> \" " + afterChange+" \" ");

		beforeChange = cmGETTEXTbyXpath(wordPressConstants.MyProfilePageConstants.Aboutme);
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.Aboutme, mapData.get("AboutMe")+randomAplbha);
		afterChange = cmGETTEXTbyXpath(wordPressConstants.MyProfilePageConstants.Aboutme);
		ExtentReporting.logger.pass("Value of Aboutme is updated from :  \" "  + beforeChange + " \" --------> \" " + afterChange+" \" ");

		beforeChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.HideToggle, "class");
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.HideToggle);
		afterChange = cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.HideToggle, "class");
		if (afterChange.contains("checked"))
			ExtentReporting.logger.pass("Hide my Gravatar profile is changed from Un-check to check");
		else
			ExtentReporting.logger.pass("Hide my Gravatar profile is changed from check to Un-check");

		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Saveprofile);
		ExtentReporting.logger.pass("The alert message displayed as : "
				+ cmGETTEXTbyXpath(wordPressConstants.MyProfilePageConstants.alrtNotice));

		cmSCREENshot("Editprofile1");
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.alrtNoticeClose);
	}

	public void VerifyUserProfileInfo() throws InterruptedException {

		assertEquals(cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Firstname, "value"),
				mapData.get("FirstName")+randomAplbha);
		ExtentReporting.logger.pass("FirstName is successfully verified as : "+ mapData.get("PublicDisplayName")+randomAplbha);
		assertEquals(cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Lastname, "value"),
				mapData.get("LastName")+randomAplbha);
		ExtentReporting.logger.pass("LastName is successfully verified as : "+ mapData.get("PublicDisplayName")+randomAplbha);
		assertEquals(cmGETATTRIBUTEValuebyXpath(wordPressConstants.MyProfilePageConstants.Publicdisplayname, "value"),
				mapData.get("PublicDisplayName")+randomAplbha);
		ExtentReporting.logger.pass("PublicDisplyname is successfully verified as : "+ mapData.get("PublicDisplayName")+randomAplbha);
		assertEquals(cmGETTEXTbyXpath(wordPressConstants.MyProfilePageConstants.Aboutme), mapData.get("AboutMe")+randomAplbha);
		ExtentReporting.logger.pass("AboutMe is successfully verified as : "+ mapData.get("PublicDisplayName")+randomAplbha);
	}

	public void AddProfileLinksSite() throws InterruptedException {
		try {
			cmSCROLLToElement(wordPressConstants.MyProfilePageConstants.Profilelinksadd);
			cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Profilelinksadd);
			ExtentReporting.logger.pass("Clicked on add button");
			cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.ProfilelinkAddsite);
			ExtentReporting.logger.pass("Clicked on add site button");
			cmSCREENshot("site1");
			cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.chkSitebox);
			ExtentReporting.logger.pass("Clicked on site checkbox");
			cmSCREENshot("site2");
			cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.btnAddsite);
			ExtentReporting.logger.pass("clicked on Add site button");
			if (!(cmFINDelementsbyxpath(wordPressConstants.MyProfilePageConstants.lblProfilelinktitleSite)
					.size() == 1)) {
				assertTrue(false);
			}
			ExtentReporting.logger.pass("Site Title is added successfully");

		} catch (Exception | AssertionError ex) {
			try {
				cmSCREENshot("site3");
				cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.btnCancelAddSite);
				assertFalse(true);
			} catch (Exception e) {
				exceptionHandling(e);
			}
		}
	}

	public void AddProfileLinksURL() throws InterruptedException, IOException {
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Profilelinksadd);
		ExtentReporting.logger.pass("Clicked on add button");
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.ProfilelinkAddurl);
		ExtentReporting.logger.pass("Clicked on add url");
		cmSCREENshot("url1");
		ExtentReporting.logger
				.pass("The default value shown in addprofile url field is : " + cmGETATTRIBUTEValuebyXpath(
						wordPressConstants.MyProfilePageConstants.txtprofileaddurl, "placeholder"));
		ExtentReporting.logger
				.pass("The default value shown in addprofile desc field is : " + cmGETATTRIBUTEValuebyXpath(
						wordPressConstants.MyProfilePageConstants.txtprofileaddurldesc, "placeholder"));
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.txtprofileaddurl, "https://www.google.co.in");
		ExtentReporting.logger.pass("new url added as https://www.google.co.in ");
		cmSETbyXpath(wordPressConstants.MyProfilePageConstants.txtprofileaddurldesc, "Google");
		ExtentReporting.logger.pass("new url description added as Google");
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.btnAddsite);
		ExtentReporting.logger.pass("clicked on Add site button");
		cmSCREENshot("link1");
		if (!(cmFINDelementsbyxpath(wordPressConstants.MyProfilePageConstants.lblProfilelinktitleURL).size() == 1)) {
			assertTrue(false);
		}
		ExtentReporting.logger.pass("URL is added successfully");
	}

	public void NavigateToProfileLinks() throws InterruptedException, IOException {
		String parentWindow = driver.getWindowHandle();
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.lnkSite);
		ExtentReporting.logger.pass("clicked on site link");
		cmSWITCHtoChildwindow(parentWindow);
		cmFINDelementbyxpath("//a[text()='Home']");
		ExtentReporting.logger.pass("Navigate to Site successfully");
		cmSCREENshot("site");
		cmSWITCHtoParentwindow(parentWindow);
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.lnkUrl);
		ExtentReporting.logger.pass("clicked on URL link");
		cmSWITCHtoChildwindow(parentWindow);
		cmFINDelementbyxpath("//input[@name='q']");
		ExtentReporting.logger.pass("Navigate to URL successfully");
		cmSCREENshot("google");
		cmSWITCHtoParentwindow(parentWindow);
	}

	public void DeleteProfilelinksWebsites() throws InterruptedException {
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.btnProfilelinkDelete);
		ExtentReporting.logger.pass("Started deleting the url and site");
		try {
			if (cmFINDelementsbyxpath(wordPressConstants.MyProfilePageConstants.alrtNotice).size() == 1) {
				ExtentReporting.logger.error("An excepton occured on deletion");
				assertTrue(false);
			}
		} catch (Exception | AssertionError e) {
			try {
				cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.btnProfilelinkDelete);
				ExtentReporting.logger.pass("url and site are deleted");
				cmSCROLLToElement("//*[@class='profile-links__no-links']");
			} catch (Exception | AssertionError ex) {
				ExtentReporting.logger.error("An excepton occured on deletion");
				assertTrue(false);
			}
		}
	}

}
