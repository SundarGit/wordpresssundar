package wordPress.Pages;

import wordPress.ElementXpaths.wordPressConstants;
import wordPress.Utilities.BaseClass;
import wordPress.Utilities.ExtentReporting;

public class LoginPage extends BaseClass {

	public void Login() throws InterruptedException {
		driver.get(prop.getProperty("url"));
		cmCLICKElementbyXpath(wordPressConstants.LoginPageConstants.lnkLogin);
		ExtentReporting.logger.pass("Navigated to url :" + prop.getProperty("url"));
		cmSETbyXpath(wordPressConstants.LoginPageConstants.UserNameEmail, prop.getProperty("userid"));
		ExtentReporting.logger.pass("Userid entered ");
		cmCLICKElementbyXpath(wordPressConstants.LoginPageConstants.Continue);
		ExtentReporting.logger.pass("Clicked on Continue Button");
		cmWAITforElement(wordPressConstants.LoginPageConstants.btnLogin);
		cmSETbyXpath(wordPressConstants.LoginPageConstants.Password, decrypt(prop.getProperty("encryptedPassword")));
		ExtentReporting.logger.pass("Password entered");
		cmCLICKElementbyXpath(wordPressConstants.LoginPageConstants.btnLogin);
		ExtentReporting.logger.pass("Clicked on Login Button");
		WaitForPageLoad();
		cmWAITforElement(wordPressConstants.MyHomePageConstants.lblMyhome);
		cmWAITforElement(wordPressConstants.MyProfilePageConstants.Myprofileicon);
		ExtentReporting.logger.pass("Successfully navigated to home page");
	}
}
