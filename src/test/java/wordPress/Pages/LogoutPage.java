package wordPress.Pages;

import static org.testng.Assert.assertTrue;

import wordPress.ElementXpaths.wordPressConstants;
import wordPress.Utilities.BaseClass;
import wordPress.Utilities.ExtentReporting;

public class LogoutPage extends BaseClass {

	public void Logout() throws InterruptedException {
		cmCLICKElementbyXpath(wordPressConstants.MyProfilePageConstants.Myprofileicon);
		cmWAITforElement(wordPressConstants.MyProfilePageConstants.Myprofile);
		String currentPage = driver.getTitle();
		assertTrue(currentPage.contains("Profile"));
		cmCLICKElementbyXpath(wordPressConstants.LogoutPageConstants.btnLogout);
		ExtentReporting.logger.pass("Clicked on Logout button");
		cmFINDelementbyxpath("//a[@title='Get Started']");
		ExtentReporting.logger.pass("verified the presence of Login button, once logged out");
	}
}
