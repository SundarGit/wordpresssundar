package wordPress.Utilities;

/*
 * This class file consist of common excel methods
 * Created on: 08-Aug-2021
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelUtilities {

	public static LinkedHashMap<String, String> getExcelDataAsMap(String excelFileName, String sheetName)
			throws EncryptedDocumentException, IOException {
		Workbook wb = WorkbookFactory
				.create(new File(System.getProperty("user.dir") + "/TestData/" + excelFileName + ".xlsx"));
		System.out.println(System.getProperty("user.dir") + "/TestData/" + excelFileName + ".xlsx");
		Sheet s = wb.getSheet(sheetName);
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		int rowCount = s.getPhysicalNumberOfRows();
		for (int i = 1; i < rowCount; i++) {
			Row r = s.getRow(i);
			String fieldName = r.getCell(0).getStringCellValue();
			String fieldValue = r.getCell(1).getStringCellValue();
			data.put(fieldName, fieldValue);
		}
		wb.close();
		return data;
	}

}

