package wordPress.Utilities;

/*
 * This class file consist of all report related methods
 * Created on: 08-Aug-2021
 */

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import wordPress.Utilities.BaseClass;

public class ExtentReporting extends TestListenerAdapter {
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest logger;

	public ExtentReporting() {

		String repName = "Test-Report.html";

//		 specify location of the report 
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/Reports/" + repName);
		htmlReporter.loadXMLConfig(System.getProperty("user.dir") + "/extent-config.xml");

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host name", "localhost");
		extent.setSystemInfo("Environemnt", "QA");

		htmlReporter.config().setDocumentTitle("Test Project"); // Tile of report
		htmlReporter.config().setReportName("Test Automation Report"); // name of the report
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP); // location of the chart
		htmlReporter.config().setTheme(Theme.STANDARD);
	}

	public void failureLogs(String path) {
		File f = new File(path);
		if (f.exists()) {
			logger.info("<a href='file:/" + path + "'>Logs.html</a>");

		}
	}

	public void Logfilechck() {
		String path = (System.getProperty("user.dir") + "/Logs/Testlogs.html");
		File f = new File(path);
		if (f.exists()) {
			extent.setSystemInfo("LogFile", "<a href='file:/" + path + "'>TestLogs</a>");
		}
	}

	public void videoFilechck() {
		String path = (System.getProperty("user.dir") + "/TestExecVideos/TestExecVideo.mov");
		File f = new File(path);
		if (f.exists()) {
			extent.setSystemInfo("TestExecutionVideo", "<a href='file:/" + path + "'>ExecVideo</a>");
		}
	}

	public void onTestSuccess(ITestResult tr) {
		try {
			captureScreen(tr.getName());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		// send the passed information to the report with GREEN color highlighted
		logger.log(Status.PASS, MarkupHelper.createLabel(tr.getName(), ExtentColor.GREEN));

		String screenshotPath = System.getProperty("user.dir") + "\\Screenshots\\" + tr.getName() + ".png";
		File f = new File(screenshotPath);
		if (f.exists()) {
			try {
				logger.addScreenCaptureFromPath(screenshotPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void captureScreen(String tname) throws IOException {

		TakesScreenshot ts = (TakesScreenshot) BaseClass.driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		File target = new File(System.getProperty("user.dir") + "/Screenshots/" + tname + ".png");
		FileUtils.copyFile(source, target);
		BaseClass.logs.info("********-Took Screenshot Successfully-********");
	}

	public void onTestFailure(ITestResult tr) {
		try {
			captureScreen(tr.getName());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		// send the passed information to the report with GREEN color highlighted
		logger.log(Status.FAIL, MarkupHelper.createLabel(tr.getName(), ExtentColor.RED));

		String screenshotPath = System.getProperty("user.dir") + "\\Screenshots\\" + tr.getName() + ".png";
		File f = new File(screenshotPath);
		if (f.exists()) {
			try {
				logger.addScreenCaptureFromPath(screenshotPath);
				logger.info("For more detail info, please refer the below log file");
				String path = (System.getProperty("user.dir") + "/Logs/Testlogs.html");
				failureLogs(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void onTestSkipped(ITestResult tr) {
		logger = extent.createTest(tr.getName()); // create new entry in th report
		logger.log(Status.SKIP, MarkupHelper.createLabel(tr.getName(), ExtentColor.ORANGE));
		logger.warning("This test case not executed as the pre-requiste testcase got failed");
	}

	public void onFinish(ITestContext testContext) {
		Logfilechck();
		videoFilechck();
		extent.flush();
		BaseClass.logs.info(
				"***********************************************************************************************************************");
		BaseClass.logs.info(
				"***                                                                                                                 ***");
		BaseClass.logs.info("      TestReport in the location :" + System.getProperty("user.dir")
				+ "\\test-output\\Test-Report.html           ");
		BaseClass.logs.info(
				"***                                                                                                                 ***");
		BaseClass.logs.info(
				"***********************************************************************************************************************");
	}
}
