package wordPress.Utilities;

import static org.testng.Assert.assertFalse;

/*
 * This class file consist of all reusable methods to utilize in all the test cases class files
 * Created on: 08-Aug-2021
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import com.google.common.base.Function;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
import io.github.bonigarcia.wdm.WebDriverManager;
import jakarta.xml.bind.DatatypeConverter;

public class BaseClass extends ExcelUtilities {
	public static WebDriver driver;
	public static Properties prop = new Properties();
	public static Logger logs;
	public static FluentWait<WebDriver> wait;
	public static WebDriverWait waitforpage;
	public static JavascriptExecutor js;
	public static ATUTestRecorder recorder;
	public String recordExecution;
	public WebDriverWait pageWait;

//	Method to create new test  in report, and take screenshot before begin the test
	@BeforeMethod
	public void ExtentReportTestCreation(Method txt) throws IOException, ATUTestRecorderException {
		try {
			logs.info(txt.getName() + " execution started ");
			ExtentReporting.logger = ExtentReporting.extent.createTest(txt.getName()); // create new entry in the report
			cmSCREENshot(txt.getName() + "Begin");
		} catch (Exception e) {
			logs.error("An exception occured on  ExtentReportTestCreation as :" + e.getMessage());
		}
	}

//	Method to clean directories before start test, initiate property file, initiate recorder, set browser
	@BeforeSuite
	public void cmDriverSetup() throws IOException, ATUTestRecorderException {
		try {
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/Logs"));
			System.out.println(System.getProperty("user.dir") + "/Logs" + " folder content deleted");
			logs = Logger.getLogger("Testing log");
			PropertyConfigurator.configure("log4j.properties");
			logs.setLevel(Level.ALL);
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/data.properties");
			prop.load(fis);
			String browserName = prop.getProperty("browser");
			String isHeadless = (prop.getProperty("headlessBrowser")).toLowerCase();
			String record = (prop.getProperty("recordExecution")).toLowerCase();
			if (isHeadless.equals("yes") && record.equals("yes")) {
				logs.warn(
						"******************************************************************************************************************");
				logs.warn(
						"******************************************************************************************************************");
				logs.warn(
						" !!CAUTION!! Not able to start the execution as Headless and RecordVideo must not be set as \"YES\" at sametime");
				logs.warn(
						"******************************************************************************************************************");
				logs.warn(
						"******************************************************************************************************************");
				System.exit(0);
			}
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/ScreenShots"));
			logs.info(System.getProperty("user.dir") + "/ScreenShots" + " folder content deleted");
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/Reports"));
			logs.info(System.getProperty("user.dir") + "/Reports" + " folder content deleted");
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/TestExecVideos"));
			logs.info(System.getProperty("user.dir") + "/TestExecVideos" + " folder content deleted");
			logs.info(
					"******************************************************************************************************************");
			logs.info(
					"******************************************************************************************************************");
			logs.info("                  The supplied browser is " + browserName.toUpperCase() + "   with headless as "
					+ isHeadless.toUpperCase());
			logs.info(
					"******************************************************************************************************************");
			logs.info(
					"******************************************************************************************************************");

			switch (browserName.toLowerCase()) {
			case "chrome":
				WebDriverManager.chromedriver().setup();
				if (isHeadless.equals("yes")) {
					ChromeOptions chromOptions = new ChromeOptions();
					chromOptions.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
							"--ignore-certificate-errors", "--silent");
					driver = new ChromeDriver(chromOptions);
				} else {
					driver = new ChromeDriver();
				}
				break;
			case "firefox":
				WebDriverManager.firefoxdriver().setup();
				if (isHeadless.equals("yes")) {
					FirefoxOptions firefoxOptions = new FirefoxOptions();
					firefoxOptions.setHeadless(true);
					driver = new FirefoxDriver(firefoxOptions);
				} else {
					driver = new FirefoxDriver();
				}
				break;
			case "edge":
				WebDriverManager.edgedriver().setup();
				if (isHeadless.equals("yes")) {
					logs.info("****************************************************");
					logs.info("Edge HeadlessBrowser Not yet implemented");
					logs.info("Please try with chrome or Edge");
					logs.info("****************************************************");
					System.exit(0);
				} else {
					driver = new EdgeDriver();
				}
				break;
			default:
				logs.error("Please enter a valid browserName");
			}
			driver.manage().window().maximize();
			recordExecution = prop.getProperty("recordExecution").toLowerCase();
			if (recordExecution.equals("yes")) {
				recorder = new ATUTestRecorder(System.getProperty("user.dir") + "\\TestExecVideos\\", "TestExecVideo",
						false);
				recorder.start();
			}
		} catch (Exception e) {
			logs.error("An exception occured on setting browser : " + e.getMessage());
		}

	}

//	Method to wait for a element, using xpath and fluentwait
	public void cmWAITforElement(String xpath) {
		wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(Duration.ofSeconds(2));
		wait.withTimeout(Duration.ofSeconds(8));
		wait.ignoring(Exception.class); // We need to ignore this exception.;

		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver arg0) {
				logs.debug("Checking for the object **" + xpath);
				WebElement element = arg0.findElement(By.xpath(xpath));
				if (element != null) {
					logs.debug("Object found." + xpath);
				}
				return element;
			}
		};
		wait.until(function);
	}

//	Method to click on element using xpath, internally call fluent wait method
	public void cmCLICKElementbyXpath(String xpath) throws InterruptedException {
		cmWAITforElement(xpath);
		driver.findElement(By.xpath(xpath)).click();
	}

//	Method to find  element using xpath, internally call fluent wait method
	public WebElement cmFINDelementbyxpath(String xpath) {
		cmWAITforElement(xpath);
		return driver.findElement(By.xpath(xpath));
	}

//	Method to find  elements using xpath, internally call fluent wait method
	public List<WebElement> cmFINDelementsbyxpath(String xpath) {
		cmWAITforElement(xpath);
		return driver.findElements(By.xpath(xpath));
	}

//	Method to take screenshot and save it with the given name
	public void cmSCREENshot(String screenshotname) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		File target = new File(System.getProperty("user.dir") + "/Screenshots/" + screenshotname + ".png");
		FileUtils.copyFile(source, target);
		logs.trace("********-Took Screenshot Successfully-********");
		String screenshotPath = System.getProperty("user.dir") + "\\Screenshots\\" + screenshotname + ".png";
		ExtentReporting.logger.addScreenCaptureFromPath(screenshotPath);
	}

//	Method to switch to parent window from child window
	public void cmSWITCHtoParentwindow(String parentWindow) {

		for (String handle : driver.getWindowHandles()) {
			if (!handle.equals(parentWindow)) {
				driver.switchTo().window(handle);
				driver.close();
			}
		}

		driver.switchTo().window(parentWindow);
	}

//	Method to switch to child window from parent window
	public void cmSWITCHtoChildwindow(String parentWindow) {

		for (String handle : driver.getWindowHandles()) {
			if (!handle.equals(parentWindow)) {
				driver.switchTo().window(handle);
			}
		}
	}

//	Method to scroll to particular element using xpath and javascriptexecutor
	public void cmSCROLLToElement(String xpath) {
		cmWAITforElement(xpath);
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath(xpath))).build().perform();
	}

	// AddedMethod to decrypt user credentials
	public String decrypt(String encryptedString) {
		String decrptData = encryptedString;
		byte[] decodeBytes = Base64.getDecoder().decode(decrptData.getBytes());
		String decodeddata = new String(decodeBytes);
		return decodeddata;
	}

	// AddedMethod to encrypt user credentials
	public String encrypt(String Stringtoencrypt) {
		String Data = Stringtoencrypt;
		byte[] encodedBytes = Data.getBytes(StandardCharsets.UTF_8);
		String encodeddata = DatatypeConverter.printBase64Binary(encodedBytes);
		return encodeddata;
	}

	@AfterMethod
	public void afterMethod(Method txt) {
		logs.info(txt.getName() + " execution completed ");
	}

	// Method to set the value using xpath
	public void cmSETbyXpath(String xpath, String value) throws InterruptedException {
		cmWAITforElement(xpath);
		driver.findElement(By.xpath(xpath)).clear();
		driver.findElement(By.xpath(xpath)).sendKeys(value);
	}

//	Method to get the value using xpath
	public String cmGETTEXTbyXpath(String xpath) throws InterruptedException {
		cmWAITforElement(xpath);
		return driver.findElement(By.xpath(xpath)).getText().toString();
	}

//	Method to get the value using xpath and attribute name
	public String cmGETATTRIBUTEValuebyXpath(String xpath, String AttributeName) throws InterruptedException {
		cmWAITforElement(xpath);
		return driver.findElement(By.xpath(xpath)).getAttribute(AttributeName.toLowerCase()).toString();
	}

//	Method to close the browser, stop the recorder and quit the driver
	@AfterSuite
	public void cmTEARdown() throws ATUTestRecorderException, InterruptedException {
		logs.info("All tests are exected, hence closing the browser ");
		driver.quit();
		if (recordExecution.equals("yes"))
			recorder.stop();
	}

	public void exceptionHandling(Throwable e) {
		ExtentReporting.logger.error("Test failed due to an Exception thrown as : " + e);
		assertFalse(true);
	}

	public void WaitForPageLoad() {
		pageWait = new WebDriverWait(driver, 50);
		pageWait.until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
				.executeScript("return document.readyState").equals("complete"));

	}

}
