package wordPress.Tests;

import org.testng.annotations.Test;
import wordPress.Pages.LogoutPage;

public class LogoutTest extends LogoutPage {

	@Test(dependsOnGroups = "LoginPage", priority = 9999)
	public void LogoutTestCase() {
		try {
			Logout();
		} catch (Exception e) {
			exceptionHandling(e);
		}

	}

}
