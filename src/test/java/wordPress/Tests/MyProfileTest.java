package wordPress.Tests;

import java.io.IOException;
import org.testng.annotations.Test;
import wordPress.Pages.MyProfilePage;

public class MyProfileTest extends MyProfilePage {

	@Test(groups = { "ProfilePage", "ALL" }, dependsOnGroups = "LoginPage")
	public void NavigatetoMyProfileTest() {
		try {
			NavigatetoMyProfile();
		} catch (Exception e) {
			exceptionHandling(e);
		}
	}

	@Test(dependsOnMethods = "NavigatetoMyProfileTest", priority = 1, groups = { "ProfilePage", "ALL" })
	public void VerifyProfilePageLinksTest() {
		try {
			VerifyProfilePageLinks();
		} catch (Exception e) {
			exceptionHandling(e);
		}
	}

	@Test(groups = { "ProfilePage", "ALL" }, dependsOnMethods = "NavigatetoMyProfileTest", priority = 2)
	public void EditProfileInfoUsingExcelTest() throws InterruptedException {
		try {
			EditProfileInfoUsingExcel();
		} catch (Exception e) {
			exceptionHandling(e);
		}
	}

	@Test(dependsOnMethods = "EditProfileInfoUsingExcelTest", groups = { "ProfilePage", "ALL" })
	public void VerifyUserProfileInfoTest() throws InterruptedException {
		try {
			VerifyUserProfileInfo();
		} catch (Exception e) {
			exceptionHandling(e);
		}
	}

	@Test(dependsOnMethods = "NavigatetoMyProfileTest", priority = 3, groups = { "ProfilePage", "ALL" })
	public void AddProfileLinksSiteTest() throws InterruptedException {
		AddProfileLinksSite();
	}

	@Test(dependsOnMethods = "AddProfileLinksSiteTest", groups = { "ProfilePage", "ALL" })
	public void AddProfileLinksURLTest() throws InterruptedException {
		try {
			AddProfileLinksURL();
		} catch (Exception | AssertionError e) {
			exceptionHandling(e);
		}
	}

	@Test(dependsOnMethods = { "AddProfileLinksURLTest", "AddProfileLinksSiteTest" }, groups = { "ProfilePage", "ALL" })
	public void NavigateToProfileLinksTest() throws InterruptedException, IOException {
		try {
			NavigateToProfileLinks();
		} catch (Exception e) {
			exceptionHandling(e);
		}

	}

	@Test(dependsOnMethods = { "AddProfileLinksURLTest", "AddProfileLinksSiteTest",
			"NavigateToProfileLinksTest" }, groups = { "ProfilePage", "ALL" })
	public void DeleteProfilelinksWebsitesTest() throws InterruptedException {
		DeleteProfilelinksWebsites();

	}

}
