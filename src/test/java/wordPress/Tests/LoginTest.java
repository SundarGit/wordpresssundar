package wordPress.Tests;

/*
 * This class file consist of all Login Page related test cases
 * Created on: 08-Aug-2021
 * 
 */

import org.testng.annotations.Test;
import wordPress.Pages.LoginPage;

public class LoginTest extends LoginPage {

	@Test(priority = 1, groups = { "LoginPage" })
	public void LoginTestCase() {
		try {
			Login();
		} catch (Exception e) {
			exceptionHandling(e);
			System.exit(0);
		}
	}
}