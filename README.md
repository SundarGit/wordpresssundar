## wordPress Assessment
## The above prjoect is a Maven project  with TestNG framework. 
- Tech used : TestNG, Maven, Eclipse IDE, Java
- The test cases are explained in Testcase.xlsx.

#### In data.properites file, please use the  options
    - browser = chrome / edge / firefox
    - headlessbrowser = no/yes 
    - recordExecution = no/yes
     

#### On successfull execution :
    - Report will be generated in "Reports" folder using ExtentReport jars
    - Screenshots will be generated in "ScreenShots" folder
    - TestExecutionVideo recording will be generated in "TestExecVideos" folder ( only when we execute using recordExection as 'yes')
    - Log file will be generated in "Logs" folder


#### Please refer "Test-Report.html" in Report folder to see the test execution result in details
    - The same file consist of link to "logs", "ExecutionVideos", "Screenshots" and step results
